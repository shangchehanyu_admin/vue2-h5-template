import axios from "axios";

const http = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 6000,
  // headers: { "X-Custom-Header": "foobar" },
});
console.log(http);
export default http;
