import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/api/request";
import "amfe-flexible"; //设置可伸缩布局

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
